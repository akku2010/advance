export interface IRegion {
    description: Array<IRegionDescription>;
}


export interface IRegionDescription {
    // id: number;
    // region: string;
    // summary: string;

    KM: any
    Speed: any
    Fuel: any
    Weight: any
    Currency: any
}