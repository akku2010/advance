import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Platform } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';
import { SocialSharing } from '@ionic-native/social-sharing';
import { File } from '@ionic-native/file';
import * as XLSX from 'xlsx';

@IonicPage()
@Component({
  selector: 'page-ignition-report',
  templateUrl: 'ignition-report.html',
})
export class IgnitionReportPage implements OnInit {

  islogin: any;
  Ignitiondevice_id: any = [];
  igiReport: any[] = [];

  datetimeEnd: any;
  datetimeStart: string;
  devices: any;
  isdevice: string;
  portstemp: any;
  datetime: any;
  igiReportData: any[] = [];
  locationEndAddress: any;
  selectedVehicle: any;
  twoMonthsLater: any = moment().subtract(2, 'month').format("YYYY-MM-DD");
  today: any = moment().format("YYYY-MM-DD");
  pltStr: string;

  constructor(
    public file: File,
    public socialSharing: SocialSharing,
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicalligi: ApiServiceProvider,
    public toastCtrl: ToastController,
    private geocoderApi: GeocoderProvider,
    private plt: Platform
    ) {
      if(this.plt.is('android')){
        this.pltStr = 'md';
      } else if(this.plt.is('ios')) {
        this.pltStr = 'ios';
      }
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);

    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
  }

  ngOnInit() {
    this.getdevices();
  }

  getdevices() {
    var baseURLp = this.apicalligi.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apicalligi.startLoading().present();
    this.apicalligi.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apicalligi.stopLoading();
        this.devices = data;
        this.portstemp = data.devices;
      },
        err => {
          this.apicalligi.stopLoading();
          console.log(err);
        });
  }
  OnExport = function () {
    let sheet = XLSX.utils.json_to_sheet(this.igiReportData);
    let wb = {
      SheetNames: ["export"],
      Sheets: {
        "export": sheet
      }
    };

    let wbout = XLSX.write(wb, {
      bookType: 'xlsx',
      bookSST: false,
      type: 'binary'
    });

    function s2ab(s) {
      let buf = new ArrayBuffer(s.length);
      let view = new Uint8Array(buf);
      for (let i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    }

    let blob = new Blob([s2ab(wbout)], { type: 'application/octet-stream' });
    let self = this;

    // this.getStoragePath().then(function (url) {
    self.file.writeFile(self.file.dataDirectory, "ignition_report_download.xlsx", blob, { replace: true })
      .then((stuff) => {
        // alert("file downloaded at: " + self.file.dataDirectory);
        if (stuff != null) {
          // self.socialSharing.share('CSV file export', 'CSV export', url, '')
          self.socialSharing.share('CSV file export', 'CSV export', stuff['nativeURL'], '')
        } else return Promise.reject('write file')
      }).catch(() => {
        alert("error creating file at :" + self.file.dataDirectory);
      });
    // });
  }

  changeDate(key) {
    this.datetimeStart = undefined;
    if (key === 'today') {
      this.datetimeStart = moment({ hours: 0 }).format();
    } else if (key === 'yest') {
      this.datetimeStart = moment().subtract(1, 'days').format();
    } else if (key === 'week') {
      this.datetimeStart = moment().subtract(1, 'weeks').endOf('isoWeek').format();
    } else if (key === 'month') {
      this.datetimeStart = moment().startOf('month').format();
    }
  }

  getIgnitiondevice(selectedVehicle) {
    console.log("selectedVehicle=> ", selectedVehicle)
    // this.Ignitiondevice_id = selectedVehicle.Device_Name;

    this.Ignitiondevice_id = [];
    if (selectedVehicle.length > 0) {
      if (selectedVehicle.length > 1) {
        for (var t = 0; t < selectedVehicle.length; t++) {
          this.Ignitiondevice_id.push(selectedVehicle[t].Device_Name)
        }
      } else {
        this.Ignitiondevice_id.push(selectedVehicle[0].Device_Name)
      }
    } else return;
    console.log("selectedVehicle=> ", this.Ignitiondevice_id)
  }

  getIgnitiondeviceReport() {

    if (this.Ignitiondevice_id.length === 0) {
      this.Ignitiondevice_id = [];

    }
    let that = this;
    this.apicalligi.startLoading().present();

    this.apicalligi.getIgiApi(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), this.Ignitiondevice_id, this.islogin._id)
      .subscribe(data => {
        this.apicalligi.stopLoading();
        this.igiReport = data;
        if (this.igiReport.length > 0) {
          this.innerFunc(this.igiReport);
        } else {
          let toast = this.toastCtrl.create({
            message: 'Report(s) not found for selected Dates/Vehicle.',
            duration: 1500,
            position: 'bottom'
          })
          toast.present();
        }


      }, error => {
        this.apicalligi.stopLoading();
        console.log(error);
      })
  }

  innerFunc(igiReport) {
    let outerthis = this;
    outerthis.igiReportData = [];
    outerthis.locationEndAddress = undefined;
    var i = 0, howManyTimes = igiReport.length;
    function f() {

      outerthis.igiReportData.push({
        'vehicleName': outerthis.igiReport[i].vehicleName,
        'switch': outerthis.igiReport[i].switch,
        'timestamp': outerthis.igiReport[i].timestamp,
        'start_location': {
          'lat': outerthis.igiReport[i].lat,
          'long': outerthis.igiReport[i].long
        }
      });
      outerthis.start_address(outerthis.igiReport[i], i);
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }

    }
    f();
  }

  // start_address(item, index) {
  //   let that = this;
  //   that.igiReportData[index].StartLocation = "N/A";
  //   if (!item.start_location) {
  //     that.igiReportData[index].StartLocation = "N/A";
  //   } else if (item.start_location) {
  //     this.geocoderApi.reverseGeocode(Number(item.start_location.lat), Number(item.start_location.long))
  //       .then((res) => {
  //         console.log("test", res)
  //         var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
  //         that.saveAddressToServer(str, item.start_location.lat, item.start_location.long);
  //         that.igiReportData[index].StartLocation = str;
  //       })
  //   }
  // }

    start_address(item, index) {
      let that = this;
      if (!item.start_location) {
        that.igiReportData[index].StartLocation = "N/A";
        return;
      }
      let tempcord =  {
        "lat" : item.start_location.lat,
        "long": item.start_location.long
      }
      this.apicalligi.getAddress(tempcord)
        .subscribe(res=> {
        console.log("test");
        console.log("result", res);
        console.log(res.address);
        // that.allDevices[index].address = res.address;
        if(res.message == "Address not found in databse")
        {
          this.geocoderApi.reverseGeocode(item.start_location.lat, item.start_location.long)
         .then(res => {
          var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
          that.saveAddressToServer(str, item.start_location.lat, item.start_location.long);
          that.igiReportData[index].StartLocation = str;
          // console.log("inside", that.address);
        })
        } else {
          that.igiReportData[index].StartLocation = res.address;
        }
      })
  }
  saveAddressToServer(address, lat, lng) {
    let payLoad = {
      "lat": lat,
      "long": lng,
      "address": address
    }
    this.apicalligi.saveGoogleAddressAPI(payLoad)
      .subscribe(respData => {
        console.log("check if address is stored in db or not? ", respData)
      },
        err => {
          console.log("getting err while trying to save the address: ", err);
        });
  }

}
